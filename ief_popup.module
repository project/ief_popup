<?php

/**
 * @file
 * Hook implementations for the IEF Complex Widget Dialog module.
 */

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\inline_entity_form\Plugin\Field\FieldWidget\InlineEntityFormComplex;
use Drupal\layout_builder\Form\ConfigureBlockFormBase;

/**
 * Implements hook_preprocess_page().
 */
function ief_popup_preprocess_page(array &$variables) {
  $variables['#attached']['library'][] = 'ief_popup/ief_popup';
}

/**
 * Implements hook_field_widget_third_party_settings_form().
 */
function ief_popup_field_widget_third_party_settings_form(
  WidgetInterface $plugin,
  FieldDefinitionInterface $field_definition,
  $form_mode,
  array $form,
  FormStateInterface $form_state,
) {
  $element = [];

  // Add a 'my_setting' checkbox to the settings form for 'foo_widget' field
  // widgets.
  if ($plugin instanceof InlineEntityFormComplex) {
    $element['ief_popup_enabled'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable Popup for IEF'),
      '#default_value' => $plugin->getThirdPartySetting('ief_popup', 'ief_popup_enabled'),
    ];
  }

  return $element;
}

/**
 * Implements hook_field_widget_settings_summary_alter().
 */
function ief_popup_field_widget_settings_summary_alter(array &$summary, array $context) {
  // Append a message to the summary when an instance of inline_entity_form has
  // ief_popup_enabled set to TRUE for the current view mode.
  if ($context['widget']->getThirdPartySetting('ief_popup', 'ief_popup_enabled')) {
    $summary[] = t('Display the form in a popup.');
  }
}

/**
 * Implements hook_field_widget_single_element_WIDGET_TYPE_form_alter().
 */
function ief_popup_field_widget_single_element_inline_entity_form_complex_form_alter(array &$element, FormStateInterface $form_state, array $context) {
  $widget = $context['widget'];

  if ($widget->getThirdPartySetting('ief_popup', 'ief_popup_enabled')) {
    $triggering_element = $form_state->getTriggeringElement();
    $items = $context['items'];
    $field_name = $items->getName();

    if (isset($triggering_element['#ief_row_form']) && $triggering_element['#ief_row_form'] === 'remove') {
      $parents = $triggering_element['#parents'];

      // Remove layout builder block parents from IEF parents.
      if ($block_parents = $form_state->getTemporaryValue('block_form_parents')) {
        $parents = array_diff_assoc($parents, $block_parents);
      }

      $ief_field = reset($parents);
      if ($ief_field !== $field_name) {
        return;
      }

      $ief_row_delta = $triggering_element['#ief_row_delta'];
      $form_object = $form_state->getFormObject();

      // Special case for layout builder.
      if ($form_object instanceof ConfigureBlockFormBase) {
        $entity = $items->getParent()->getEntity();
        if (!isset($element['entities'][$ief_row_delta]['form']['#entity']) && isset($element['entities'][$ief_row_delta]['#entity'])) {
          $element['entities'][$ief_row_delta]['form']['#entity'] = $element['entities'][$ief_row_delta]['#entity'];
        }
      }
      else {
        $entity = $form_object->getEntity();
      }

      // Determine parameters based on the entity type.
      $parameters = [];
      switch ($entity->getEntityTypeId()) {
        case 'block_content':
          $parameters = [
            'parent_bundle_label' => $entity->type->entity->label(),
            'parent_entity_title' => $entity->label(),
          ];
          break;

        case 'node':
          $parameters = [
            'parent_bundle_label' => $entity->type->entity->label(),
            'parent_entity_title' => $entity->getTitle(),
          ];
          break;

        case 'taxonomy_term':
          $parameters = [
            'parent_bundle_label' => $entity->type->entity->label(),
            'parent_entity_title' => $entity->getName(),
          ];
          break;

        case 'user':
          $parameters = [
            'parent_bundle_label' => 'user',
            'parent_entity_title' => $entity->getDisplayName(),
          ];
          break;
      }

      ief_popup__process_ief_form($element['entities'][$ief_row_delta]['form'], 'remove', $parameters);
    }

    if (isset($triggering_element['#ief_form']) && $triggering_element['#ief_form'] === 'ief_add_existing') {
      ief_popup__process_ief_form($element['form'], 'existing');
    }
  }
}

/**
 * Implements hook_inline_entity_form_alter().
 */
function ief_popup_inline_entity_form_entity_form_alter(array &$ief_form, FormStateInterface &$form_state) {
  // Get the field name of the current IEF form.
  $ief_popup_enabled = FALSE;
  /** @var \Drupal\Core\Entity\Entity\EntityFormDisplay $form_display */
  $form_display = $form_state->getStorage()['form_display'];

  if (!empty($form_display)) {
    foreach ($form_display->getComponents() as $widget) {
      if (array_key_exists('ief_popup', $widget['third_party_settings']) && $widget['third_party_settings']['ief_popup']['ief_popup_enabled'] == 1) {
        $ief_popup_enabled = TRUE;
        break;
      }
    }
  }

  if ($ief_popup_enabled) {
    ief_popup__process_ief_form($ief_form, $ief_form["#op"]);
  }
}

/**
 * Function that makes the actual adaptation to the IEF form.
 *
 * @param array $ief_form
 *   The IEF form that needs to be processed.
 * @param string $ief_action
 *   The IEF action (add, remove, existing, edit, duplicate).
 * @param array $parameters
 *   Parameters for $ief_action "remove".
 */
function ief_popup__process_ief_form(&$ief_form, $ief_action, $parameters = []) {

  /* add wrapper to the IEF form */
  $ief_overlay_classes = [
    "ief-popup-overlay",
    "ui-widget-overlay",
  ];

  $ief_wrapper_classes = [
    "ief-popup-wrapper",
    "ief-popup-wrapper-" . $ief_action,
  ];

  if (!empty($ief_form['#ief_labels']['singular'])) {
    $ief_wrapper_classes[] = 'ief-popup-wrapper-' . $ief_form['#ief_labels']['singular'];
  }

  $ief_dialog_classes = [
    "ui-dialog",
    "ui-corner-all",
    "ui-widget",
    "ui-widget-content",
    "ui-front",
    "ui-dialog-buttons",
  ];

  $ief_form["#prefix"] = '
    <div class="' . implode(" ", $ief_overlay_classes) . '"></div>
    <div class="' . implode(" ", $ief_wrapper_classes) . '">
        <div>
            <div role="dialog" class="' . implode(" ", $ief_dialog_classes) . '">';
  $ief_form["#suffix"] = '
            </div>
       </div>
   </div>';

  /* action buttons are added after the form_alter hook so altering action buttons happens in the after_build hook */
  $ief_form["#after_build"][] = "ief_popup__ief_form_after_build";

  /* set popup_title for the title bar */
  if ($ief_action === 'edit') {
    if (isset($ief_form['title'])) {
      $entity_title = $ief_form['#default_value']->get('title')->value;
      $popup_title = t('Edit @type %title',
        [
          '@type' => $ief_form['#ief_labels']['singular'],
          '%title' => $entity_title,
        ]);
    }
    else {
      $popup_title = t('Edit @type',
        [
          '@type' => $ief_form['#ief_labels']['singular'],
        ]);
    }
  }

  elseif ($ief_action === 'duplicate') {
    if (isset($ief_form['title'])) {
      $entity_title = $ief_form['#default_value']->get('title')->value;
      $popup_title = t('Duplicate @type %title',
        [
          '@type' => $ief_form['#ief_labels']['singular'],
          '%title' => $entity_title,
        ]);
    }
    else {
      $popup_title = t('Duplicate @type',
        [
          '@type' => $ief_form['#ief_labels']['singular'],
        ]);
    }
  }
  elseif ($ief_action == "view") {
    $popup_title = t('View @type', ["@type" => $ief_form["#ief_labels"]["singular"]]);
  }
  elseif ($ief_action == "add") {
    $popup_title = t('Add @type', ["@type" => $ief_form["#ief_labels"]["singular"]]);
  }
  elseif ($ief_action == "remove") {
    $popup_title = t('Remove');
  }
  elseif ($ief_action == "existing") {
    $popup_title = $ief_form["#title"];

    /* remove the default titlebar in the ief_add_existing form */
    unset($ief_form["#title"]);
  }

  /* improve markup for remove form */
  if ($ief_action == 'remove') {
    $entity = $ief_form['#entity'];
    $entity_title = '';
    $entity_bundle = '';
    if ($entity) {
      $entityTypeId = $entity->getEntityTypeId();
      switch ($entityTypeId) {
        case 'block_content':
          $entity_title = $entity->label();
          $entity_bundle = 'block_content';
          break;

        case 'user':
          $entity_title = $entity->getDisplayName();
          $entity_bundle = 'user';
          break;

        case 'node':
          $entity_title = $entity->getTitle();
          $entity_bundle = $entity->bundle();
          break;

        case 'taxonomy_term':
          $entity_title = $entity->getName();
          $entity_bundle = $entity->bundle();
          break;
      }
    }

    // Check if the keys exist in the $parameters array and set default values
    // if they are not present.
    $parent_bundle_label = isset($parameters['parent_bundle_label']) ? strtolower($parameters['parent_bundle_label']) : '';
    $parent_entity_title = $parameters['parent_entity_title'] ?? '';

    if ($parent_bundle_label !== '' || $parent_entity_title !== '') {
      if (isset($ief_form['message']['#overriden']) && ($ief_form['message']['#overriden'] === FALSE)) {
        $ief_form["message"]["#markup"] =
          t("Are you sure you want to remove this @entity_bundle from this @parent_bundle_label?",
            [
              "%entity_title" => $entity_title,
              "@entity_bundle" => $entity_bundle,
              "@parent_bundle_label" => $parent_bundle_label,
              "%parent_entity_title" => $parent_entity_title,
            ]);
      }
    }
    else {
      $ief_form["message"]["#markup"] =
        t("Are you sure you want to remove %entity_title?",
          ["%entity_title" => $entity_title]);
    }

    $ief_form["delete"]["#title"] = t("Delete @type_singular <strong>permanent</strong> from the system.",
      ["@type_singular" => $entity_title]);
  }

  /* add title bar before first_field */
  $ief_form["first"]["#weight"] = -1000;
  $ief_form["first"]["#markup"] = '
    <div class="ui-dialog-titlebar ui-corner-all ui-widget-header ui-helper-clearfix">
        <span id="ui-id-3" class="ui-dialog-title">' . $popup_title . '</span>
        <a type="button" class="ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close ief-popup-close" title="Close">
            <span class="ui-button-icon ui-icon ui-icon-closethick"></span>
            <span class="ui-button-icon-space"> </span>Close
        </a>
    </div>';

  /* add wrapper around form fields */
  $ief_form["first"]["#markup"] .= '<div class="ui-dialog-content ui-widget-content">';
}

/**
 * Callback IEF after_build hook.
 */
function ief_popup__ief_form_after_build($ief_form) {

  /*
   * add some extra classes on the actions wrapper and buttons
   * .ief-popup-cancel is used in the javascript to close the popup via the 'X'
   */
  $ief_form["actions"]["#attributes"]["class"][] = "ief-popup-actions";

  $primary_buttons = [
    "ief_edit_save",
    "ief_add_save",
    "ief_reference_save",
    "ief_remove_confirm",
    "ief_duplicate_save",
  ];
  foreach ($primary_buttons as $primary_button) {
    if (isset($ief_form["actions"][$primary_button])) {
      $ief_form["actions"][$primary_button]["#attributes"]["class"][] = "button--primary";
    }
  }

  $cancel_buttons = [
    'ief_view_cancel',
    "ief_edit_cancel",
    "ief_add_cancel",
    "ief_reference_cancel",
    "ief_remove_cancel",
    "ief_duplicate_cancel",
  ];
  foreach ($cancel_buttons as $cancel_button) {
    if (isset($ief_form["actions"][$cancel_button])) {
      $ief_form["actions"][$cancel_button]["#attributes"]["class"][] = "ief-popup-cancel";
    }
  }

  // Close wrapper around form fields.
  $ief_form["actions"]["#prefix"] = '</div>';

  return $ief_form;
}
